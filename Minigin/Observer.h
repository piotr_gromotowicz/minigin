#pragma once

class GameObject;

enum class ObserverEvent
{
	BombExploded
};
class Observer
{
public:
	Observer();
	virtual ~Observer();
	virtual void OnNotify(const GameObject* object, ObserverEvent event) = 0;
};

