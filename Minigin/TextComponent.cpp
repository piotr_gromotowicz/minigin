#include "MiniginPCH.h"
#include <SDL.h>
#include <SDL_ttf.h>

#include "TextComponent.h"
#include "Renderer.h"
#include "GameObject.h"
#include "Transform.h"
#include "Component.h"
TextComponent::TextComponent(const std::string& text, std::shared_ptr<Font> font)
	: mNeedsUpdate(true),mText(text),mFont(font)
{
}

TextComponent::~TextComponent()
{
}

void TextComponent::Render() const
{
	if (mGameObject->GetTexture() != nullptr)
	{
		const auto pos = mGameObject->GetComponent<Transform>()->GetPosition();
		const auto texture = mGameObject->GetTexture();
		Renderer::GetInstance().RenderTexture(*texture, pos.x, pos.y);
	}
}

void TextComponent::Update()
{
	if (mNeedsUpdate)
	{
		const SDL_Color color = { 255,255,255 };
		const auto surf = TTF_RenderText_Blended(mFont->GetFont(), mText.c_str(), color);
		if (surf == nullptr) {
			std::stringstream ss; ss << "Render text failed: " << SDL_GetError();
			throw std::runtime_error(ss.str().c_str());
		}
		auto texture = SDL_CreateTextureFromSurface(Renderer::GetInstance().GetSDLRenderer(), surf);
		if (texture == nullptr) {
			std::stringstream ss; ss << "Create text texture from surface failed: " << SDL_GetError();
			throw std::runtime_error(ss.str().c_str());
		}
		SDL_FreeSurface(surf);
		mGameObject->SetTexture(std::make_shared<Texture2D>(texture));
	}
}

void TextComponent::SetText(const std::string & text)
{
	mText = text;
	mNeedsUpdate = true;
}

void TextComponent::SetFont(std::shared_ptr<Font> font)
{
	mFont = font;
	mNeedsUpdate = true;
}
