#include "MiniginPCH.h"
#include "InputManager.h"
#include <SDL.h>
#include "PlayerController.h"
#include "Command.h"

void InputManager::Init()
{

}
bool InputManager::ProcessInput()
{
	ZeroMemory(&player1currentState, sizeof(XINPUT_STATE));
	XInputGetState(0, &player1currentState);

	ZeroMemory(&player2currentState, sizeof(XINPUT_STATE));
	XInputGetState(1, &player2currentState);

	SDL_Event e;
	while (SDL_PollEvent(&e))
	{
		mCurrentFrameEvents.push_back(e);
	}

	for(auto p : mPlayersControllers)
	{
		auto command = PlayerInputHandle(p->GetPlayerIndex());
		if (command)
			command->Execute(p);
	}
	return true;
}

void InputManager::ClearEventQueue()
{
	mCurrentFrameEvents.clear();
}

Command* InputManager::PlayerInputHandle(int playerIndex) const
{
	std::vector<std::pair<ButtonPair, Command* >> keyBinding;
	if (playerIndex == 0)
		keyBinding = mKeyBindingsP1;
	else if (playerIndex == 1)
		keyBinding = mKeyBindingsP2;
	else
		return nullptr;

	for (auto k : keyBinding)
	{
		ControllerButton pC = k.first.controllerButton;
		KeyboardButton pK = k.first.keyboardButton;

		if (IsPressed(pC, playerIndex) || IsPressed(pK))
		{
			return k.second;
		}
	}
	return nullptr;
}

void InputManager::AddPlayer(std::shared_ptr<PlayerController> controller)
{
	mPlayersControllers.push_back(controller);
}

std::shared_ptr<PlayerController> InputManager::GetController(int playerIndex)
{
	for(auto p : mPlayersControllers)
	{
		if (p->GetPlayerIndex() == playerIndex)
			return p;
	}
	return nullptr;
}

bool InputManager::IsPressed(ControllerButton button, int playerIndex) const
{
	if(playerIndex == 0)
	{
		switch (button)
		{
		case ControllerButton::ButtonB:
			return player1currentState.Gamepad.wButtons & XINPUT_GAMEPAD_B;
		case ControllerButton::ButtonX:
			return player1currentState.Gamepad.wButtons & XINPUT_GAMEPAD_X;
		case ControllerButton::DpadDown:
			return player1currentState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_DOWN;
		case ControllerButton::DpadUp:
			return player1currentState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_UP;
		case ControllerButton::DpadLeft:
			return player1currentState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_LEFT;
		case ControllerButton::DpadRight:
			return player1currentState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_RIGHT;
		default: return false;
		}
	}
	if(playerIndex == 1)
	{
		switch (button)
		{
		case ControllerButton::ButtonB:
			return player2currentState.Gamepad.wButtons & XINPUT_GAMEPAD_B;
		case ControllerButton::ButtonX:
			return player2currentState.Gamepad.wButtons & XINPUT_GAMEPAD_X;
		case ControllerButton::DpadDown:
			return player2currentState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_DOWN;
		case ControllerButton::DpadUp:
			return player2currentState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_UP;
		case ControllerButton::DpadLeft:
			return player2currentState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_LEFT;
		case ControllerButton::DpadRight:
			return player2currentState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_RIGHT;
		default: return false;
		}
	}
	return false;
}
bool InputManager::IsPressed(KeyboardButton button) const
{
	for(auto e : mCurrentFrameEvents)
	{
		if(e.key.state == SDL_PRESSED)
		{
			switch (button)
			{
			case KeyboardButton::KeyA:
				return e.key.keysym.scancode == SDL_SCANCODE_A;
			case KeyboardButton::KeyD:
				return e.key.keysym.scancode == SDL_SCANCODE_D;
			case KeyboardButton::KeyW:
				return e.key.keysym.scancode == SDL_SCANCODE_W;
			case KeyboardButton::KeyS:
				return e.key.keysym.scancode == SDL_SCANCODE_S;
			case KeyboardButton::KeyG:
				return e.key.keysym.scancode == SDL_SCANCODE_G;
			case KeyboardButton::KeyJ:
				return e.key.keysym.scancode == SDL_SCANCODE_J;
			case KeyboardButton::KeyArrowUp:
				return e.key.keysym.scancode == SDL_SCANCODE_UP;
			case KeyboardButton::KeyArrowDown:
				return e.key.keysym.scancode == SDL_SCANCODE_DOWN;
			case KeyboardButton::KeyArrowLeft:
				return e.key.keysym.scancode == SDL_SCANCODE_LEFT;
			case KeyboardButton::KeyArrowRight:
				return e.key.keysym.scancode == SDL_SCANCODE_RIGHT;
			case KeyboardButton::KeyNumpad1:
				return e.key.keysym.scancode == SDL_SCANCODE_KP_1;
			case KeyboardButton::KeyNumpad3:
				return e.key.keysym.scancode == SDL_SCANCODE_KP_3;
			default: return false;
			}
		}
	}
	return false;
}
void InputManager::SetButton(ButtonPair playerInputs, Command* command, int playerIndex)
{
	std::pair<ButtonPair, Command*> inputPair;
	inputPair.first = playerInputs;
	if(command)
		inputPair.second = command;
	if(playerIndex == 0)
		mKeyBindingsP1.push_back(inputPair);
	if(playerIndex == 1)
		mKeyBindingsP2.push_back(inputPair);

}

