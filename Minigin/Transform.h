#pragma once
#pragma warning(push)
#pragma warning (disable:4201)
#include <glm/vec3.hpp>
#pragma warning(pop)
#include "Component.h"

	class Transform : public Component
	{
		glm::vec3 mPosition;
	public:
		Transform() = default;
		virtual ~Transform();

		void Update();
		void Render() const;

		const glm::vec3& GetPosition() const { return mPosition; }
		void SetPosition(float x, float y, float z);
	};
