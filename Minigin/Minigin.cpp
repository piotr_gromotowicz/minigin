#include "MiniginPCH.h"
// ReSharper disable once CppUnusedIncludeDirective
#include <vld.h>
#pragma comment(lib,"xinput.lib")
#include "SDL.h"
#include "ResourceManager.h"
#include "Renderer.h"
#include "RenderComponent.h"
#include "TextComponent.h"
#include <chrono>
#include <thread>
#include "InputManager.h"
#include "SceneManager.h"

#include "Scene.h"
#include "GameObject.h"
#include "Transform.h"
#include "PlayerController.h"
#include "Command.h"
const int msPerFrame = 16; //16 for 60 fps, 33 for 30 fps

void Initialize();
void InitializeKeybinds();
void LoadGame();
void Cleanup();

#pragma warning( push )  
#pragma warning( disable : 4100 )  
int main(int argc, char* argv[]) {
#pragma warning( pop )

	Initialize();

	// tell the resource manager where he can find the game data
	ResourceManager::GetInstance().Init("../Data/");

	LoadGame();

	{
		auto t = std::chrono::high_resolution_clock::now();
		auto& renderer = Renderer::GetInstance();
		auto& sceneManager = SceneManager::GetInstance();
		auto& input = InputManager::GetInstance();
	
		bool doContinue = true;
		auto lastTime = std::chrono::high_resolution_clock::now();
		float lag = 0.0f;
		while(doContinue) 
		{
			auto currentTime = std::chrono::high_resolution_clock::now();
			float deltaTime = std::chrono::duration<float>(currentTime - lastTime).count();
			lastTime = currentTime;
			lag += deltaTime;

			doContinue = input.ProcessInput();

			while (lag >= (msPerFrame/1000.f))
			{
				lag -= (msPerFrame/1000.f);
				sceneManager.Update();
			}
			
			renderer.Render();

			input.ClearEventQueue();
			t += std::chrono::milliseconds(msPerFrame);
			std::this_thread::sleep_until(t);
		}
	}
	Cleanup();
    return 0;
}

SDL_Window* window;

void Initialize()
{
	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		std::stringstream ss; ss << "SDL_Init Error: " << SDL_GetError();
		throw std::runtime_error(ss.str().c_str());
	}

	window = SDL_CreateWindow(
		"Programming 4 assignment",
		SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED,
		640,                    
		480,                    
		SDL_WINDOW_OPENGL       
	);
	if (window == nullptr) {
		std::stringstream ss; ss << "SDL_CreateWindow Error: " << SDL_GetError();
		throw std::runtime_error(ss.str().c_str());
	}

	Renderer::GetInstance().Init(window);
	InitializeKeybinds();
}
void InitializeKeybinds()
{
	auto& a = InputManager::GetInstance();
	
	//Player1
	ButtonPair buttonPair;
	buttonPair.controllerButton = ControllerButton::DpadLeft;
	buttonPair.keyboardButton = KeyboardButton::KeyA;
	a.SetButton(buttonPair, new Left(),0);
}
void LoadGame()
{
	auto& scene = SceneManager::GetInstance().CreateScene("Demo");
	
	auto go = std::make_shared<GameObject>();
	
	auto renderer = std::make_shared<RenderComponent>();
	go->SetTexture("background.jpg");
	go->AddComponent(renderer);

	scene.Add(go);

	auto trans = go->GetComponent<Transform>();
	trans->Update();

	go = std::make_shared<GameObject>();
	go->SetPosition(216, 180);

	renderer = std::make_shared<RenderComponent>();
	go->SetTexture("logo.png");
	go->AddComponent(renderer);

	scene.Add(go);

	go = std::make_shared<GameObject>();
	go->SetPosition(80, 20);

	auto font = ResourceManager::GetInstance().LoadFont("Lingua.otf", 36);
	auto textComponent = std::make_shared<TextComponent>("Programming 4 Assignment", font);
	go->AddComponent(textComponent);

	auto player = std::make_shared<PlayerController>();
	InputManager::GetInstance().AddPlayer(player);
	go->AddComponent(player);
	scene.Add(go);
}

void Cleanup()
{
	Renderer::GetInstance().Destroy();
	SDL_DestroyWindow(window);
	SDL_Quit();
}

