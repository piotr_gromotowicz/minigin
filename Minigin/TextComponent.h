#pragma once
#include "Texture2D.h"
#include "Component.h"
#include "Font.h"
class TextComponent : public Component
{
public:
	TextComponent(const std::string& text, std::shared_ptr<Font> font);
	~TextComponent();

	void Render() const;
	void Update();

	void SetText(const std::string& text);
	void SetFont(std::shared_ptr<Font> font);

private:
	bool mNeedsUpdate;
	std::string mText;
	std::shared_ptr<Font> mFont;

};