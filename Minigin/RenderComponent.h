#pragma once
#include "Component.h"
class Texture2D;
class RenderComponent : public Component
{
	
public:
	RenderComponent();
	~RenderComponent();

	void Render() const;
	void Update();
};

