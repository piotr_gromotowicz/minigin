#pragma once
#include "Observer.h"

class Audio :
	public Observer
{
public:
	Audio();
	~Audio();
	void OnNotify(const GameObject* object, ObserverEvent event) override;

private:
	void PlayAudio();

	bool mBombExploded;
};

