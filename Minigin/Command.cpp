#include "MiniginPCH.h"
#include "Command.h"
#include "PlayerController.h"
void Up::Execute(std::shared_ptr<PlayerController> player)
{
	player->MoveUp();
}

void Down::Execute(std::shared_ptr<PlayerController> player)
{
	player->MoveDown();
}

void Left::Execute(std::shared_ptr<PlayerController> player)
{
	player->MoveLeft();
}

void Right::Execute(std::shared_ptr<PlayerController> player)
{
	player->MoveRight();
}
void Place::Execute(std::shared_ptr<PlayerController> player)
{
	player->PlaceBomb();
}
void Detonate::Execute(std::shared_ptr<PlayerController> player)
{
	player->DetonateBomb();
}
