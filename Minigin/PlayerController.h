#pragma once
#include "Component.h"

class PlayerController :
	public Component
{
public:
	PlayerController();
	~PlayerController();

	void Update() override;
	void Render() const;
	
	int GetPlayerIndex();
	void MoveUp();
	void MoveDown();
	void MoveLeft();
	void MoveRight();
	void PlaceBomb();
	void DetonateBomb();

private:
	static int mPlayers;
	int mPlayerIndex;
};

