#include "MiniginPCH.h"
#include "GameObject.h"
#include "ResourceManager.h"
#include "Renderer.h"
#include "Transform.h"
#include "Component.h"
#include "Subject.h"
GameObject::~GameObject() = default;



GameObject::GameObject() 
{
	//Initialize transform for every game object to 0
	mTransform = std::make_shared<Transform>();
	mTransform->SetPosition(0.f, 0.f, 0.f);
	mComponents.push_back(mTransform);

	mGameObjectChange = std::make_unique<Subject>();
	mGameObjectChange->
};
void GameObject::RootRender() const
{
	for (std::shared_ptr<Component> c : mComponents)
	{
		c->Render();
	}
}

void GameObject::RootUpdate()
{
	for (std::shared_ptr<Component> c : mComponents)
	{
		c->Update();
	}
}

void GameObject::SetTexture(const std::string& filename)
{
	mTexture = ResourceManager::GetInstance().LoadTexture(filename);
}

void GameObject::SetTexture(const std::shared_ptr<Texture2D> texture)
{
	mTexture = texture;
}

void GameObject::SetPosition(float x, float y)
{
	mTransform->SetPosition(x, y, 0.0f);
}

void GameObject::SetTag(const std::string& tag)
{
	mTag = tag;
}

std::string GameObject::GetTag() const
{
	return mTag;
}

void GameObject::AddComponent(const std::shared_ptr<Component>& component)
{
	for (auto c : mComponents)
	{
		if (typeid(*c) == typeid(*component))
			return;
	}
	component->mGameObject = this;
	mComponents.push_back(component);
}
