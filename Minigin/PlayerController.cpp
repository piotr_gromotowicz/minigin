#include "MiniginPCH.h"
#include "PlayerController.h"
#include "GameObject.h"
#include "Transform.h"
#include "Component.h"

int PlayerController::mPlayers = 0;

PlayerController::PlayerController()
{
	mPlayerIndex = mPlayers;
	mPlayers++;
}

PlayerController::~PlayerController()
{
}

void PlayerController::Update()
{


}
void PlayerController::Render() const
{
	
}

int PlayerController::GetPlayerIndex()
{
	return mPlayerIndex;
}

void PlayerController::MoveUp()
{

}

void PlayerController::MoveDown()
{
	
}

void PlayerController::MoveLeft()
{
	auto transform = this->mGameObject->GetComponent<Transform>();
	this->mGameObject->SetPosition(transform->GetPosition().x + 1, transform->GetPosition().y);
}

void PlayerController::MoveRight()
{
}

void PlayerController::PlaceBomb()
{
}

void PlayerController::DetonateBomb()
{
}
