#pragma once
#include <memory>
class Component;
class Transform;
class Texture2D;
class Subject;
	class GameObject
	{
	public:
		virtual void Update() = 0;
		virtual void Render() const = 0;
		void RootRender() const;
		void RootUpdate();
		void SetTexture(const std::string& filename);
		void SetTexture(const std::shared_ptr<Texture2D> texture);
		void SetPosition(float x, float y);
		void SetTag(const std::string& tag);

		std::string GetTag() const;
		std::shared_ptr<Texture2D> GetTexture() const { return mTexture; }
		void AddComponent(const std::shared_ptr<Component>& component);

#pragma region

		template <class T>
		std::shared_ptr<T> GetComponent() const
		{
			for (auto c : mComponents)
			{
				if (std::shared_ptr<T> ptr = std::dynamic_pointer_cast<T>(c))
					return ptr;
			}
			return nullptr;
		}
#pragma endregion Templated Component Methods

		GameObject();
		virtual ~GameObject();
		GameObject(const GameObject& other) = delete;
		GameObject(GameObject&& other) = delete;
		GameObject& operator=(const GameObject& other) = delete;
		GameObject& operator=(GameObject&& other) = delete;

	private:
		std::vector<std::shared_ptr<Component>> mComponents;
		std::shared_ptr<Texture2D> mTexture;
		std::shared_ptr<Transform> mTransform;
		std::string mTag;
		std::unique_ptr<Subject> mGameObjectChange;
	};