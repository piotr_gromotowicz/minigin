#include "MiniginPCH.h"
#include "Audio.h"
#include "Observer.h"
#include "GameObject.h"
Audio::Audio()
{
}


Audio::~Audio()
{
}

void Audio::OnNotify(const GameObject* object, ObserverEvent event)
{
	switch (event)
	{
	case ObserverEvent::BombExploded:
		if (object->GetTag().compare("Bomb") || mBombExploded)
		{
			PlayAudio();
		}
		break;
	}
	mBombExploded = false;
}

void Audio::PlayAudio()
{
}
