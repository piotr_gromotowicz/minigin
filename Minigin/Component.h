#pragma once
class GameObject;
class Component
{
public:
	Component();
	virtual ~Component() = default;

	virtual void Update() = 0;
	virtual void Render() const = 0;

	GameObject* GetGameObject() const { return mGameObject; }

	Component(const Component& other) = delete;
	Component(Component&& other) = delete;
	Component& operator=(const Component& other) = delete;
	Component& operator=(Component&& other) = delete;
	
private:
	friend class GameObject;
protected:
	GameObject* mGameObject;
};