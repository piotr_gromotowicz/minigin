#include "MiniginPCH.h"
#include "RenderComponent.h"
#include "Renderer.h"
#include "ResourceManager.h"
#include "GameObject.h"
#include "Component.h"
#include "Transform.h"
RenderComponent::RenderComponent()
{
}


RenderComponent::~RenderComponent()
{
}

void RenderComponent::Render() const
{
	const auto pos = mGameObject->GetComponent<Transform>()->GetPosition();
	const auto texture = mGameObject->GetTexture();
	Renderer::GetInstance().RenderTexture(*texture, pos.x, pos.y);
}

void RenderComponent::Update()
{
}
