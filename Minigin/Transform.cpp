#include "MiniginPCH.h"
#include "Transform.h"

Transform::~Transform() = default;

void Transform::Update()
{
}

void Transform::Render() const
{
}

void Transform::SetPosition(const float x, const float y, const float z)
{
	mPosition.x = x;
	mPosition.y = y;
	mPosition.z = z;
}
