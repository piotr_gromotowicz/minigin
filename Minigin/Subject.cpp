#include "MiniginPCH.h"
#include "Subject.h"
#include "Observer.h"

Subject::Subject()
{
}


Subject::~Subject()
{
}

void Subject::AddObserver(Observer* observer)
{
	mObservers.push_back(observer);
}

void Subject::RemoveObserve(Observer* observer)
{
	for(auto i = 0; i < mObservers.size(); ++i)
	{
		if (typeid(mObservers[i]) == typeid(observer))
		{
			mObservers.erase(mObservers.begin() + i);
			return;
		}
	}
}


void Subject::Notify(const GameObject* object, ObserverEvent event)
{
	for(auto o : mObservers)
	{
		o->OnNotify(object, event);
	}
}
