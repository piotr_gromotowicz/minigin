#pragma once
#include <XInput.h>
#include "Singleton.h"
#include <SDL.h>
class PlayerController;
class Command;

enum class ControllerButton
	{
		ButtonB,
		ButtonX,
		DpadDown,
		DpadUp,
		DpadLeft,
		DpadRight
	};
	enum class KeyboardButton
	{
		KeyW,
		KeyA,
		KeyS,
		KeyD,
		KeyG,
		KeyJ,
		KeyArrowUp,
		KeyArrowLeft,
		KeyArrowDown,
		KeyArrowRight,
		KeyNumpad1,
		KeyNumpad3
	};
	struct ButtonPair
	{
		ControllerButton controllerButton;
		KeyboardButton keyboardButton;
	};
	class InputManager final : public Singleton<InputManager>
	{
	public:
		void Init();
		bool ProcessInput();
		void ClearEventQueue();
		Command* PlayerInputHandle(int playerIndex) const;
		void AddPlayer(std::shared_ptr<PlayerController> controller);
		std::shared_ptr<PlayerController> GetController(int playerIndex);
		bool IsPressed(ControllerButton button, int playerIndex) const;
		bool IsPressed(KeyboardButton button) const;
		void SetButton(ButtonPair buttonPair , Command* command, int playerIndex);
	private:
		XINPUT_STATE player1currentState{};
		XINPUT_STATE player2currentState{};

		std::vector < std::pair<ButtonPair, Command*>> mKeyBindingsP1;
		std::vector < std::pair<ButtonPair, Command*>> mKeyBindingsP2;

		std::vector<SDL_Event> mCurrentFrameEvents;
		std::vector<std::shared_ptr<PlayerController>> mPlayersControllers;
	};