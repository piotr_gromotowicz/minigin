#pragma once

class PlayerController;

class Command
{
public:
	virtual ~Command() = default;
	virtual void Execute(std::shared_ptr<PlayerController> player) = 0;
};

class Up : public Command
{
public:
	void Execute(std::shared_ptr<PlayerController> player) override;
};
class Down : public Command
{
public:
	void Execute(std::shared_ptr<PlayerController> player) override;
};
class Left : public Command
{
public:
	void Execute(std::shared_ptr<PlayerController> player) override;
};
class Right : public Command
{
public:
	void Execute(std::shared_ptr<PlayerController> player) override;
};
class Place : public Command
{
public:
	void Execute(std::shared_ptr<PlayerController> player) override;
};
class Detonate : public Command
{
public:
	void Execute(std::shared_ptr<PlayerController> player) override;
};
