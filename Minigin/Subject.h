#pragma once
#include <vector>
#include "Observer.h"

class GameObject;
class Subject
{
public:
	Subject();
	~Subject();

	void AddObserver(Observer* observer);
	void RemoveObserve(Observer* observer);
private:
	void Notify(const GameObject* object, ObserverEvent event);

	std::vector<Observer*> mObservers;
};

